express = require "express"
fs = require "fs"
less = require "less-middleware"
bcrypt = require "bcrypt"
eco = require "eco"
limiter = require "connect-ratelimit"

PASSWORD_FILE = "that_password.txt"

app = express()

app.configure ->
  app.set "port", process.env.PORT or 8000
  app.use express.favicon()
  app.use express.bodyParser()
  app.use express.methodOverride()
  app.use less
    src: "less"
    dest: "public"
    compress: yes
    paths: "less"
  app.use express.static("public")
  app.use limiter()

app.get "/", (req, res) ->
  res.sendfile "public/index.html"

app.get "/admin", (req, res) ->
  fs.exists PASSWORD_FILE, (exists) ->
    if !exists
      res.sendfile("adminpublic/new.html")
    else
      fs.readFile "adminpublic/edit.eco", "utf-8", (err1, template) ->
        fs.readFile "public/index.html", "utf-8", (err2, toEdit) ->
          res.send eco.render template,
            html: toEdit

app.post "/admin_new_password", (req, res) ->
  pass = req.body.pass
  if pass.trim()
    fs.exists PASSWORD_FILE, (exists) ->
      if !exists
        storeThis = bcrypt.hashSync(pass, bcrypt.genSaltSync(10))
        fs.writeFileSync(PASSWORD_FILE, storeThis, "utf8")
      res.redirect "/admin"
  else
    res.redirect "/admin"

app.post "/admin_save", (req, res) ->
  pass = req.body.pass
  html = req.body.html
  fs.readFile PASSWORD_FILE, "utf-8", (err, realPass) ->
    if bcrypt.compareSync(pass, realPass)
      fs.writeFile "public/index.html", html, "utf8", ->
        res.redirect "/"
    else
      res.redirect "/admin"

app.listen(app.get "port")
console.log "App started in #{app.get "env"} mode on port #{app.get 'port'}"
