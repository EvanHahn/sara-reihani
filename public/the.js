var originalBackground;
function updateBackground(multiplier) {

	// Selectors
	var $html = $("html");

	// Define the original background if it's not
	if (originalBackground == null)
		originalBackground = $html.css("background-color");

	// What's the "max" color? At noon, we'll hit this color.
	var maxColor = new RGBColor(originalBackground);

	// What's our multiplier? Midnight = 0, noon = 1.
	if (multiplier == null) {
		var now = moment();
		var noon = moment().seconds(0).minutes(0).hours(12);
		var diffFromNoon = Math.abs(now.diff(noon));
		var maxDiff = Math.abs(noon.diff(moment().endOf("day")));
		multiplier = 1 - (diffFromNoon / maxDiff);
	}

	// Calculate the background color
	var r = Math.floor(maxColor.r * multiplier);
	var g = Math.floor(maxColor.g * multiplier);
	var b = Math.floor(maxColor.b * multiplier);

	// Go go go!
	$html.css("background-color", "rgb("+r+","+g+","+b+")");

}

$(document).ready(function() {

	// Enable smooth scrolling (requires plugin)
	$("a").smoothScroll();

	// Add .future or .past class for IRL dates
	// This enables filtering
	$("#irl .date").each(function() {
		var $this = $(this);
		var date = new Date($this.text());
		var now = new Date;
		var future = date > now;
		if (future)
			$this.parent().addClass("future");
		else
			$this.parent().addClass("past");
	});

	// Deal with the sortables
	$("section").each(function() {

		var $section = $(this);
		var sortableString = $section.data().sortable;

		if (sortableString) {

			var $sortable = $(document.createElement("ul"));
			$sortable.addClass("sortable");

			var sortables = sortableString.split(" ");

			for (var i = 0; i < sortables.length; i ++) {

				var name = sortables[i];

				var li = document.createElement("li");

				var $label = $(document.createElement("label"))
				             .attr("for", name).html(name);

				var $checkbox = $(document.createElement("input"))
				                .attr("type", "checkbox")
				                .attr("checked", "checked")
				                .attr("name", name).val(name);

				$checkbox.on("change", function() {
					var $this = $(this);
					var checked = $this.is(':checked');
					var value = $this.val();
					$("." + value, $section).toggle(checked);
				});

				li.appendChild($checkbox[0]);
				li.appendChild($label[0]);

				$sortable[0].appendChild(li);

			}

			$("h1", this).after($sortable);

		}

	});

	// Update the background
	updateBackground();

});
