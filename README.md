Sara Reihani's website
======================

Compile `the.less` and put it in the same directory as `index.html`.

License stuff
-------------

The code that _isn't_ taken from the stuff below is licensed under the Unlicense (reproduced below). This page has help from the following:

* [LESS](http://lesscss.org/) - Apache License
* [Eric Meyer's CSS reset 2.0](http://meyerweb.com/eric/tools/css/reset/) - Public domain
* [normalize.css 2.1.0](http://git.io/normalize) - MIT License
* [Fluidable Grid System 0.8](http://fluidable.com/) - Public domain
* [Lato font](http://www.latofonts.com/) - SIL Open Font License 1.1
* [jQuery 1.9.0](http://www.jquery.org/) - MIT License
* [jQuery Smooth Scroll plugin](https://github.com/kswedberg/jquery-smooth-scroll) - MIT License
* [Moment.js](http://momentjs.com/) - MIT License
* [RGB Color](http://www.phpied.com/rgb-color-parser-in-javascript/) - "Use it if you like it"
* [HTML5 Shiv pre3.6](https://github.com/aFarkas/html5shiv) - MIT License
* [box-sizing Polyfill](https://github.com/Schepp/box-sizing-polyfill) - GNU Lesser General Public License

Unlicense
---------

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or distribute this software, either in source code form or as a compiled binary, for any purpose, commercial or non-commercial, and by any means.

In jurisdictions that recognize copyright laws, the author or authors of this software dedicate any and all copyright interest in the software to the public domain. We make this dedication for the benefit of the public at large and to the detriment of our heirs and successors. We intend this dedication to be an overt act of relinquishment in perpetuity of all present and future rights to this software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>.
